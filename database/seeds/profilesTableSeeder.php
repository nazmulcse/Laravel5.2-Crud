<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class profilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limit=50;
        $faker = Faker::create();
        for($i=1;$i<=$limit;$i++){
            DB::table('profiles')->insert([
                'type_id' => $faker->randomDigitNotNull,
                'name' => $faker->name,
                'email' => $faker->email,
                'address' => $faker->address,
                'contact' => $faker->phoneNumber
            ]);
        }
    }
}
