<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table='profiles';
    protected $fillable=['type_id','name','email','address','contact','dates'];
    protected $dates=['dates'];
    public function setDatesAttribute($date){
        $this->attributes['dates']=Carbon::CreateFromFormat('d-m-Y',$date)->format('Y-m-d');
    }
    public function getDatesAttribute($date){
        return Carbon::CreateFromFormat('Y-m-d',$date)->format('d-m-Y');
    }
}
