<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/','ProfileController@getIndex');
    Route::get('/profile/print','PrintController@index');
    Route::get('/profile/test','ProfileController@test');
    Route::controller('profile','ProfileController');
    Route::get('/search','ProfileController@search');
    Route::delete('profile/destroy/{id}','ProfileController@deleteDestroy')->name('profile.destroy');
});