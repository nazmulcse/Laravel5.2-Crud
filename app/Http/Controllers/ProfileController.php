<?php

namespace App\Http\Controllers;
use Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Profile;
use Illuminate\Support\Facades\DB;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Toastr;

class ProfileController extends Controller
{
    public function getIndex(){
        Breadcrumbs::addBreadcrumb('Home','/');
        Breadcrumbs::addBreadcrumb('Index','/');
        $profiles=Profile::all();
        return  view('Profile.index',compact('profiles'));
    }
    public function getCreate(){
        $type=DB::table('types')->lists('types','id');
        return view('Profile.create',compact('type'));
    }
    public function postStore(Requests\ProfileFormRequest $request){
        try {
            Profile::create($request->all());
            Toastr::success(config('app_config.msg_save'), "Save", $options = []);
            return redirect('/profile/create');

        } catch (\Exception $e) {

            Toastr::error(config('app_config.msg_failed_save'), "Save", $options = []);
            return redirect('/profile/create');
        }
    }
    public function getShow($id){
        $profile=Profile::find($id);
        //dd($profile);
        return view('Profile.view',compact('profile'));
    }
    public function getEdit($id){
        $profile=Profile::find($id);
        $type=DB::table('types')->lists('types','id');
        return view('Profile.edit',compact('profile','type'));
    }
    public function patchUpdate(Requests\ProfileFormRequest $request,$id){
        $profile=Profile::find($id);
        $input=$request->all();
        //dd($profile);
        $profile->update($input);
        return  redirect('/');
    }
    public function deleteDestroy($id){
        $profiles=Profile::findOrFail($id);
        $profiles->delete();
        return  redirect('/');
    }
    public function test(Request $requests){
        $data=$requests->input('name');
        dd($data);
        return view('Profile.test',compact('data'));
    }
    public function search(){
        $profile=Profile::all();
        $companies = array("name"=>"Java","id"=>"C++");
        echo json_encode( $companies );
    }
}
