<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;

class PrintController extends Controller
{
    public function index(){
//        $pdf = App::make('dompdf.wrapper');
        $pdf=PDF::loadView('printView');
        return $pdf->stream();
    }
}
