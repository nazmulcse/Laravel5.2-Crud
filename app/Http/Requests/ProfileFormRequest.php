<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:profiles,name,'.$this->id,
            'email'=> 'required|min:10',
            'address'=> 'required|min:3',
            'contact'=> 'required|min:3'
        ];
    }
    public function all()
    {
        $input = parent::all();
        $input['name']=$input['name'].$input['last_name'];
        return $input;
    }

    public function messages()
    {
        return [
            'name'=>'Provide Full Name',
            'email'=>'Provide Email'
        ];
    }
}
