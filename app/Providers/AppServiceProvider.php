<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
//use Illuminate\Support\Facades\view;
use View;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
class AppServiceProvider extends ServiceProvider
{
    public function composer(){
        View::composer('Breadcrumb.breadcrumbs', function() {

            $data = [
                'global_breadcrumbs' => Breadcrumbs::automatic()
            ];

            view()->share($data);
        });
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composer();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
