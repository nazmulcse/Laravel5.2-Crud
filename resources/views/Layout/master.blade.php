<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Simple CRUD</title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/toastr.min.css" rel="stylesheet">
    <link href="/assets/css/select2.min.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="section">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/profile/create">SignUp<span class="sr-only">(current)</span></a></li>
                    <li><a href="#">Link</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>
<div class="row">
    <div class="row col-md-3"></div>
    <div class="row col-md-3">
        {{--@include('Breadcrumb.breadcrumbs')--}}
    </div>
</div>
<div class="row">
    <div class="row col-md-3"></div>
    @yield('content')
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/toastr.min.js"></script>
<script src="/assets/js/select2.min.js"></script>
<script src="/assets/js/jquery-ui.min.js"></script>
{!! Toastr::render() !!}
<script>
    $(function() {
        $( ".nameId" ).autocomplete({
            source: "/search",
            minLength: 1
        });
    });
</script>
<script type="application/javascript">

    $(function(){
        $("#btnGo").on('click',function(e){
            e.preventDefault(e)
            window.open("/profile/test", '_blank');
            // window.location.replace("/profile/test", '_blank');
        });
    });
    $(function() {


        // Basic
        $('.select').select2();


        //
        // Select with icons
        //

        // Initialize
        $(".select-icons").select2({
            formatResult: iconFormat,
            minimumResultsForSearch: "-1",
            width: '100%',
            formatSelection: iconFormat,
            escapeMarkup: function(m) { return m; }
        });

        // Format icons
        function iconFormat(state) {
            var originalOption = state.element;
            return "<i class='icon-" + $(originalOption).data('icon') + "'></i>" + state.text;
        }



        // Styled form components
        // ------------------------------

        // Checkboxes, radios
        $(".styled").uniform({ radioClass: 'choice' });

        // File input
        $(".file-styled").uniform({
            fileButtonHtml: '<i class="icon-googleplus5"></i>',
            wrapperClass: 'bg-warning'
        });

    });
</script>
</body>
</html>