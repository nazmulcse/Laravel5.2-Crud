@extends('Layout.master')
@section('content')

    <div class="row col-md-6">
        @foreach($errors->all() as $errors)
            <p class="alert alert-danger">{{ $errors }}</p>
        @endforeach
        {{--{!! Form::open(array('url' => 'profile/store')) !!}--}}
        {!! Form::model($profile,['method' => 'patch','url'=>['profile/update',$profile->id]]) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('last_name', 'Last Name:', ['class' => 'control-label']) !!}
            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('types', 'Profile Type:') !!}
            {!! Form::select('type_id', $type,null, ['placeholder' => 'Select', 'class' => 'select form-control']) !!}
            {{--Form::select('size', array('L' => 'Large', 'S' => 'Small'));--}}
        </div>
        <div class="form-group">
            {!! Form::label('contact', 'Contact No:', ['class' => 'control-label']) !!}
            {!! Form::text('contact', null, ['class' => 'form-control']) !!}
        </div>
            {{--<div class="form-group">--}}
                {{--{!! Form::label('dates', 'Date Of Birth', ['class' => 'control-label']) !!}--}}
                {{--{!! Form::input('date','dates',Carbon\Carbon::now()->format('d-m-Y'), ['class' => 'form-control']) !!}--}}
            {{--</div>--}}
            {!! Form::hidden('id',null) !!}
            {{--<div class="form-group">--}}
                {{--{!! Form::label('dates', 'Date Of Birth', ['class' => 'control-label']) !!}--}}
                {{--{!! Form::text('dates',null, ['class' => 'form-control']) !!}--}}
                {{--@if ($errors->has('dates'))<p class="text-danger">{!! $errors->first('dates') !!}</p>@endif--}}
            {{--</div>--}}
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
    </div>
@endsection