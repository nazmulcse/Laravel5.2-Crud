@extends('Layout.master')
@section('content')
    <div class="row col-md-4">
        {{--<ul class="list-group">--}}
            {{--<li class="list-group-item">{{$profile->name}}</li>--}}
            {{--<li class="list-group-item">{{$profile->email}}</li>--}}
            {{--<li class="list-group-item">{{$profile->address}}</li>--}}
            {{--<li class="list-group-item">{{$profile->contact}}</li>--}}
        {{--</ul>--}}
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Contact No</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$profile->name}}</td>
                    <td>{{$profile->email}}</td>
                    <td>{{$profile->address}}</td>
                    <td>{{$profile->contact}}</td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection