@extends('Layout.master')
@section('content')
    <div class="row col-md-7">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Contact No</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($profiles as $profile)
            <tr>
                <td>{{$profile->name}}</td>
                <td>{{$profile->email}}</td>
                <td>{{$profile->address}}</td>
                <td>{{$profile->contact}}</td>
                <td>
                    <a href="{{url('profile/print')}}" target="_blank" class="btn btn-primary">Print</a>
                    <a href="{{url('profile/show',[$profile->id])}}" class="btn btn-primary">View</a>
                    <a href="{{url('profile/edit',[$profile->id])}}" class="btn btn-primary">Edit</a>
                    {{--{!! Form::open(['method' => 'DELETE', 'route'=>['profile.destroy', $profile->id]]) !!}--}}
                    {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                    {{--{!! Form::close() !!}--}}
                    {!! Form::open([
                            'method'=>'DELETE',
                            'route'=>['profile.destroy',$profile->id]
                        ]) !!}
                    {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection