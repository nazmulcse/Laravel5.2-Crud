
@extends('Layout.master')
@section('content')
    <div class="row col-md-6">
        @if(Session::has('message'))
            <div class="alert alert-success">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
        @if(Session::has('msgFailed'))
            <div class="alert alert-danger">
                <p>{{ Session::get('msgFailed') }}</p>
            </div>
        @endif
        {{--@foreach($errors->all() as $errors)--}}
            {{--<p class="alert alert-danger">{{ $errors }}</p>--}}
        {{--@endforeach--}}
        {!! Form::open(array('url' => 'profile/store','id'=>'frmId','method'=>'post')) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'nameId form-control']) !!}
            @if ($errors->has('name'))<p class="text-danger">{!! $errors->first('name') !!}</p>@endif
        </div>
        <div class="form-group">
            {!! Form::label('last_name', 'Last Name:', ['class' => 'control-label']) !!}
            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
            @if ($errors->has('email'))<p class="text-danger">{!! $errors->first('email') !!}</p>@endif
        </div>
        <div class="form-group">
            {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
            @if ($errors->has('address'))<p class="text-danger">{!! $errors->first('address') !!}</p>@endif
        </div>
        <div class="form-group">
            {!! Form::label('types', 'Profile Type:') !!}
            {!! Form::select('type_id', $type,null, ['placeholder' => '', 'class' => 'select form-control']) !!}
            {{--Form::select('size', array('L' => 'Large', 'S' => 'Small'));--}}
            @if ($errors->has('type_id'))<p class="text-danger">{!! $errors->first('type_id') !!}</p>@endif
        </div>
        <div class="form-group">
            {!! Form::label('contact', 'Contact No:', ['class' => 'control-label']) !!}
            {!! Form::text('contact', null, ['class' => 'form-control']) !!}
            @if ($errors->has('contact'))<p class="text-danger">{!! $errors->first('contact') !!}</p>@endif
        </div>
            {!! Form::hidden('id',null) !!}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('dates', 'Date Of Birth', ['class' => 'control-label']) !!}--}}
            {{--{!! Form::text('dates',null, ['class' => 'form-control']) !!}--}}
            {{--@if ($errors->has('dates'))<p class="text-danger">{!! $errors->first('dates') !!}</p>@endif--}}
        {{--</div>--}}

        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <button type="submit" class="btn btn-primary" id="btnGo">GO</button>

        {!! Form::close() !!}
    </div>

@endsection
