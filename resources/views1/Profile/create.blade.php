@extends('Layout.master')
@section('content')

    <div class="row col-md-4">
        @foreach($errors->all() as $errors)
            <p class="alert alert-danger">{{ $errors }}</p>
        @endforeach
        {!! Form::open(array('url' => 'profile/store')) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('contact', 'Contact No:', ['class' => 'control-label']) !!}
            {!! Form::text('contact', null, ['class' => 'form-control']) !!}
        </div>

        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
    </div>
@endsection