@extends('Layout.master')
@section('content')
    <div class="row col-md-5">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Contact No</th>
            </tr>
            </thead>
            <tbody>
            @foreach($profiles as $profile)
            <tr>
                <td>{{$profile->name}}</td>
                <td>{{$profile->email}}</td>
                <td>{{$profile->address}}</td>
                <td>{{$profile->contact}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection